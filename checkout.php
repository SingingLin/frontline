<?php
require __DIR__ . '/__connect_db.php';

if (!empty($_SESSION['cart'])) {

    $sids = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $sids));

    $rs = $mysqli->query($sql);

    $cart_data = array();

    while ($row = $rs->fetch_assoc()) {
        $row['qty'] = $_SESSION['cart'][$row['sid']];
        $cart_data[$row['sid']] = $row;
    }
}
?>

    <div class="container">
        <?php include __DIR__ . '/__html_head.php' ?>
        <style>
            .remove_item {
                font-size: x-large;
                cursor: pointer;
            }


        </style>


        <?php include __DIR__ . '/__navbar.php' ?>


        <div class="col-md-12">
            <?php if (isset($_SESSION['cart'])): ?>
                <table class="table table-bordered">
                    <thead>

                    <tr>
                        <th>商品</th>
                        <th>商品名稱</th>
                        <th>規格</th>
                        <th>數量</th>
                        <th>價格</th>
                        <th>總計</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total = 0;
                    foreach ($_SESSION['cart'] as $sid => $qty):
                        $row = $cart_data[$sid];
                        $total += $row['price']*$row['qty'];
                        ?>
                    <tr data-sid="<?= $row['sid'] ?>">
                        <td><img style="width: 170px;height: 100px;" src="imgs/small/<?= $row['head_image'] ?>.jpeg" alt=""></td>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['description_1'] ?><br>
                            <?= $row['description_2'] ?><br>
                            <?= $row['description_3'] ?><br>
                            <?= $row['description_4'] ?><br>
                            <?= $row['description_5'] ?><br>
                        </td>
                        <td>
                            <select name="qty" class="qty" data-qty="<?= $row['qty'] ?>">
                                <?php for ($i=1;$i<10;$i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                                <?php endfor; ?>
                            </select>

                        </td>
                        <td class="price"><?= $row['price'] ?></td>
                        <td class="subtotal"><?= $row['price'] * $row['qty'] ?></td>

                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <label class="radio-inline">
                    <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">信用卡:
                    <span class="total_price "><?= $total ?></span>信用卡付款 一次付清
                </label><br>
                <label class="radio-inline">
                    <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">ATM
                    <span class="total_price "><?= $total ?></span>信用卡付款 一次付清
                </label><br>
                <label class="radio-inline">
                    <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">超商:
                    <span class="total_price "><?= $total ?></span>超商付款不取貨
                </label><br>
                <div class="col-md-12">
                    <a href="checkout_form.php" class="btn btn-primary pull-right">確認</a>
                    <a href="product_list.php" class="btn btn-primary pull-right">繼續購物</a>
                </div>
            <?php else: ?>


            <?php endif; ?>


        </div>
    </div>

    <script>
        var qty_sels = $('select.qty');

        qty_sels.each(function () {
            var qty = $(this).attr('data-qty');
            $(this).val(qty);
        });

        qty_sels.change(function () {
            var $tr = $(this).closest('tr');
            var sid = $tr.attr("data-sid");
            var qty = $(this).val();
            var price = $tr.find('.price').text();
            var $subtotal = $tr.find('.subtotal');

            $.get('add_to_cart.php',{sid:sid,qty:qty},function (data) {
                $subtotal.text(qty*price);
                calc_items(data);
                calc_total_price()
            },'json')
        })

        function calc_total_price() {
            var t = 0
            $('td.subtotal').each(function () {
                t += parseInt($(this).text());
            });
            $('.total_price').text(t)
        }
    </script>

<?php include __DIR__ . '/__html_foot.php' ?>