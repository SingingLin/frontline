<?php
require __DIR__ . '/__connect_db.php';
$dir = __DIR__. '/images/';

$pname = 'personal';
$success = false;
$success_pw = false;

if(! isset($_SESSION['user'])){
    header('Location: ./');
    exit;
}

$sql = "SELECT * FROM `members` WHERE `sid`=". intval($_SESSION['user']['sid']) ;
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();

$image = $row['image'];
$email = $row['email'];
$name = $row['name'];
$gender = $row['gender'];
$phone = $row['phone'];
$address = $row['address'];
$password = $row['password'];

$file_name =  pathinfo($image, PATHINFO_EXTENSION);
$img_name = $_SESSION['user']['email'];
$target = $dir . $image;
if(empty($image)){

} else {
    rename("$target", $dir.$img_name.'.'.$file_name);
}


if(isset($_POST['email'])) {
//    echo '<pre>';
//    print_r($_POST);
//    echo '</pre> ';

    $sql = "SELECT `image` FROM `members` WHERE `sid`=". intval($_SESSION['user']['sid']) ;
    $rs = $mysqli->query($sql);
    $row = $rs->fetch_assoc();

    $image = $row['image'];

    $file_name =  pathinfo($image, PATHINFO_EXTENSION);

    $stmt = $mysqli->prepare("UPDATE `members` SET
        `image`=?,
        `email`=?,
        `name`=?,
        `gender` = ?,
        `phone` = ?,
        `address` = ?
        WHERE `sid`=?");

    $filename = $_POST['email'].".".$file_name;

    $stmt->bind_param('ssssssi',
        $filename,
        $_POST['email'],
        $_POST['name'],
        $_POST['gender'],
        $_POST['phone'],
        $_POST['address'],
        $_SESSION['user']['sid']
    );

    $success = $stmt->execute();
    $affected = $stmt->affected_rows;

    $image = $filename;
    $email = $_POST['email'];
    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];

    if($affected==1){
        $_SESSION['user']['email'] = $_POST['email'];
    }

    //echo "\$affected: $affected";
}



if(isset($_POST['newpassword'])) {
    echo '<pre>';
    print_r($_POST);
    echo '</pre> ';
    $sql = sprintf( "UPDATE `members` SET `password`= '%s' WHERE `sid`='%s' AND `password`='%s'",
        sha1($_POST['newpassword']), $_SESSION['user']['sid'], sha1($_POST['oldpassword']) );
    $success_pw =  $mysqli->query($sql);


//    $stmt = $mysqli->prepare("SELECT * FROM `members` WHERE `password`=? ");
//
//    $stmt->bind_param('s',
//        sha1($_POST['newpassword'])
//    );
//
//    $success = $stmt->execute();
//    $affected1 = $stmt->affected_rows;
//
//    if($affected1==1){
//        $_SESSION['user']['password'] = $_POST['newpassword'];
//    }

//    $sql_pw = "SELECT * FROM `members` WHERE `password`=". sha1($_POST['newpassword']) ;
//    $rs_pw = $mysqli->query($sql_pw);
//    $rs_pw->num_rows;
//
//
//    if($rs_pw->num_rows){
//        $_SESSION['user']['password'] = sha1($_POST['newpassword']);
//    }
}

if ($success_pw){
    echo ' <script> alert( "密碼變更成功" ) </script> ';
}


?>
<?php include  __DIR__. '/__html_head.php'; ?>
    <style>
        .info {
            color: red;
            font-weight: bold;
        }

        .panel-body .edit {
            display: none;
        }
        .panel-body.edit .edit {
            display: block;
        }
        .panel-body.edit .noedit {
            display: none;
        }

        .editOrder {
            display: block;
        }

        .change {
            display: none;
        }
        div.show+.change {
            display: block;
        }

        ::-webkit-input-placeholder { color: red; }

        #img_div{
            width: 200px;
        }
        #img_div img{
            width: 100%;
        }

    </style>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php'; ?>

        <?php if(isset($affected)): ?>
            <?php if($affected==1): ?>
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        個人資料修改完成
                    </div>
                </div>
            <?php elseif( $row['email'] = $_POST['email'] && $row['name'] = $_POST['name']): ?>
                <div class="alert alert-success" role="alert">
                    個人資料未修改
                </div>
            <?php else: ?>
                <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        未完成修改, 可能密碼輸入錯誤
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <div class="col-md-6">

            <div class="row">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">個人資料修改</h3></div>
                    <div class="panel-body test">

                        <div id='img_div'>
                            <!--                        <img src='images/--><?//= isset($_SESSION['user']['image']) ? $email.".jpg" : "non_member.jpg" ?><!--'>-->
                            <img src='images/<?= empty($image) ? "non_member.jpg" : $image ?>'>
                        </div>

                        <form action="file_upload.php" method="post" enctype="multipart/form-data" style="display: none;">
                            <input type="file" name="my_file" id="my_file"><br>
                        </form>

                        <button id="btn" class="edit" name="upload">test</button>
                        <a href="javascript: clear_imgsession()"><div class="cancelimg edit btn btn-default">取消</div></a>

                        <form class="" name="form1" method="post" onsubmit="return checkForm();">
                            <!--                        <div class="form-group">-->
                            <!--                            <label for="email"> <span class="info"></span></label>-->
                            <!--                            <input type="text" class="form-control" id="email" name="email"-->
                            <!--                                   value="" disabled="disabled">-->
                            <!--                        </div>-->
                            <div class="form-group">
                                <label for="email">Email <span class="info"></span></label><br/>
                                <span class="noedit email"><?= $email ?></span>
                                <input type="text" class="form-control edit email" id="email" name="email"
                                       value="<?= $email ?>">
                            </div>

                            <div class="form-group">
                                <label for="name">姓名</label><br/>
                                <span class="noedit name"><?= $name ?></span>
                                <input type="text" class="form-control edit name" id="name" name="name"
                                       value="<?= $name ?>">
                            </div>

                            <div class="form-group">
                                <label for="gender">性別</label><br/>
                                <span class="noedit gender"><?= $gender ?></span>
                                <!--                            <input type="text" class="form-control edit gender" id="gender" name="gender"-->
                                <!--                                   value="--><?//= $gender ?><!--">-->
                                <select name="gender" class="gender">
                                    <option value="1">男</option>
                                    <option value="2">女</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="phone">電話號碼</label><br/>
                                <span class="noedit phone"><?= $phone ?></span>
                                <input type="text" class="form-control edit phone" id="phone" name="phone"
                                       value="<?= $phone ?>">
                            </div>

                            <div class="form-group">
                                <label for="address">地址</label><br/>
                                <span class="noedit address"><?= $address ?></span>
                                <input type="text" class="form-control edit address" id="address" name="address"
                                       value="<?= $address ?>">
                            </div>

                            <!--                        <div class="form-group edit" style="display: none">-->
                            <!--                            <label for="password">密碼 ** <span class="info"></span></label>-->
                            <!--                            <input type="password" class="form-control edit" id="password" name="password" value="--><?//= $_SESSION['user']['password'] ?><!--">-->
                            <!--                        </div>-->

                            <!--                        <button type="submit" class="btn btn-default">修改</button>-->

                            <div class="editOrder noedit btn btn-default">編輯</div>
                            <button  type="submit" class="saveEdit edit btn btn-default">儲存</button>
                            <div class="cancelEdit edit btn btn-default">取消修改</div>
                        </form>

                        <form action="" class="test" name="form2" method="post" onsubmit="return checkPassword()">
                            <div class="change_password_btn edit btn btn-default">變更密碼</div>
                            <div class="form-group change">

                                <div>
                                    <label for="oldpassword">輸入舊密碼<span class="info"></span></label>
                                    <input type="password" class="form-control" id="oldpassword" name="oldpassword">
                                </div>

                                <div class="form-group">
                                    <label for="newpassword">輸入新密碼<span class="info"></span></label>
                                    <input type="password" class="form-control" id="newpassword" name="newpassword">
                                </div>

                                <div class="form-group">
                                    <label for="newpassword_check">再輸入一次<span class="info"></span></label>
                                    <input type="password" class="form-control" id="newpassword_check" name="newpassword_check">
                                </div>

                                <button  type="submit" class="savechange btn btn-default">確認送出</button>
                                <div class="cancelchange  btn btn-default">取消更改</div>
                            </div>
                        </form>


                    </div>
                </div>

            </div>


        </div>


    </div>
    <script>

        function clear_imgsession() {
            $('.cancelimg').click(function () {
                $.ajax()
            });
        };

        var my_file = $('#my_file');

        my_file.change(function(){
            var fd = new FormData();
            var file = $(this)[0].files[0];

            fd.append('my_file', file);

            $.ajax({
                url: 'file_upload.php',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function ( data ) {
//                    alert( data );
//                    location.href = location.href;
//                    var img = "<img src='images/" + data+ "'>";
//                    $('#img_div img').parent.location.reload();
                    $('#img_div img').attr("src","images/" + data );

                }

            });

        });

        $('#btn').click(function(){
            my_file.click();
        });

        $('.editOrder').click(function () {
            $('.test').addClass('edit');
        });

        $('.cancelEdit').click(function () {
            $('.test').removeClass('edit');
        });

        $('.change_password_btn').click(function () {
            $('.change_password_btn').addClass('show');
            $('.test').removeClass('edit');
            $('.editOrder').css('display','none');
        });

        $('.cancelchange').click(function () {
            $('.change_password_btn').removeClass('show');
            $('.editOrder').css('display','block');
        });



        function checkForm(){
            var $name = $('#name');
            var $gender = $('#gender');
            var $phone = $('#phone');
            var $address = $('#address');

            var items = [$name, $phone, $gender, $address];

            var name = $name.val();
            var gender = $gender.val();
            var phone = $phone.val();
            var address = $address.val();

            var i;
            var isPass = true;

            for(i=0; i<items.length; i++){
                items[i].css('border-color', '#ccc');
            }


            if(name.length < 2){
                $name.attr('placeholder',' 請填寫真實姓名!');
                $name.css('border-color', 'red');
                isPass = false;
            }


            if(! phone){
                $phone.attr('placeholder',' 請填寫電話 !');
                $phone.css('border-color', 'red');
                isPass = false;
            }

            if(! address){
                $address.attr('placeholder',' 請填寫地址 !');
                $address.css('border-color', 'red');
                isPass = false;
            }

            return isPass;

            if (isPass ){
                $('.saveEdit').click(function () {
                    $('.test').find('input.email').val();
                    $('.test').find('input.name').val();
                    $('.test').find('#img_div').html();
                    $('.test').removeClass('edit');
                });
            };
        };

        function checkPassword(){
            var $oldpassword = $('#oldpassword');
            var $newpassword = $('#newpassword');
            var $newpassword_check = $('#newpassword_check');

            var items = [$oldpassword, $newpassword, $newpassword_check];

            var oldpassword = $oldpassword.val();
            var newpassword = $newpassword.val();
            var newpassword_check = $newpassword_check.val();

            var i;
            var isPass = true;

            for(i=0; i<items.length; i++){
                items[i].attr('placeholder','');
                items[i].css('border-color', '#ccc');
            }


            if(oldpassword.length < 6){
                $oldpassword.val('');
                $oldpassword.attr('placeholder',' 密碼至少 6 個字元 !');
                $oldpassword.css('border-color', 'red');
                isPass = false;
            }

            if(! oldpassword){
                $oldpassword.val('');
                $oldpassword.attr('placeholder',' 請輸入舊密碼 !');
                $oldpassword.css('border-color', 'red');
                isPass = false;
            }

            if(newpassword.length < 6){
                $newpassword.val('');
                $newpassword.attr('placeholder',' 密碼至少 6 個字元 !');
                $newpassword.css('border-color', 'red');
                isPass = false;
            }

            if(! newpassword){
                $newpassword.val('');
                $newpassword.attr('placeholder',' 請輸入新密碼 !');
                $newpassword.css('border-color', 'red');
                isPass = false;
            }

            if(! (newpassword_check == newpassword) ){
                $newpassword_check.val('');
                $newpassword_check.attr('placeholder',' 密碼不一致 !');
                $newpassword_check.css('border-color', 'red');
                isPass = false;
            }

            if(! newpassword_check){
                $newpassword_check.val('');
                $newpassword_check.attr('placeholder',' 請再輸入一次 !');
                $newpassword_check.css('border-color', 'red');
                isPass = false;
            }

            $.get('aj_check_password.php', {oldpassword: oldpassword}, function(data) {
                if (data == '0') {
                    $oldpassword.val('');
                    $oldpassword.attr('placeholder', '密碼錯誤！！！！');
                    $oldpassword.css('border-color', 'red');
                    isPass = false;
//
//                    isPass = true;


//                    $('.savechange').click(function () {
//                        $('.change_password_btn').removeClass('show');
//                        $('.editOrder').css('display','block');
//                    });

                }
                if(isPass){
                    document.form2.submit();
                }
            });



            return false;

        };


    </script>
<?php include  __DIR__. '/__html_foot.php'; ?>