-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- 主機: localhost
-- 產生時間： 2017 年 03 月 26 日 20:13
-- 伺服器版本: 10.1.21-MariaDB
-- PHP 版本： 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `TEST_FrontLine`
--

-- --------------------------------------------------------

--
-- 資料表結構 `members`
--

CREATE TABLE `members` (
  `sid` int(11) NOT NULL,
  `account` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `gender` enum('男','女') NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `activated` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `team_sid` int(11) DEFAULT NULL,
  `join_num` int(11) DEFAULT NULL,
  `kill_num` int(11) DEFAULT NULL,
  `missoni_complete` int(11) DEFAULT NULL,
  `kda` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `members`
--

INSERT INTO `members` (`sid`, `account`, `password`, `name`, `nickname`, `gender`, `email`, `phone`, `hash`, `activated`, `created_at`, `team_sid`, `join_num`, `kill_num`, `missoni_complete`, `kda`) VALUES
(1, 'singinglin', '9635163036c3b186221c9cdbba5e6c8a3341a9ab', '林心韻', 'Singing', '女', 'linsinging1234@gmail.com', '0953814423', '94a3761877fd7b3cb9aec1149b8fb81b', 0, '2017-03-25 14:43:53', NULL, NULL, NULL, NULL, NULL),
(2, 'sssing', '9635163036c3b186221c9cdbba5e6c8a3341a9ab', '這是測試', 'ssssss', '男', 'linsinging@gmail.com', '0912345678', '09b967ad482053703b46fb345fcd36e9', 0, '2017-03-25 15:35:25', NULL, NULL, NULL, NULL, NULL),
(3, 'testtest', '10a07cdb61a9a8b27b7104cf5ec97eb5fa5b4d20', 'TESTTT', 'TESTTTTTT', '男', 'lin1234@gmail.com', '0977666555', '54e44c970287d4b3d01563a333449283', 0, '2017-03-25 15:37:49', NULL, NULL, NULL, NULL, NULL),
(70, 'singingaaaaaalinqqq', 'f7a9e24777ec23212c54d7a350bc5bea5477fdbb', 'aaaaaa', 'aaaaaa', '男', 'linsinging1234@gmail.comaaa', 'a', '3b14f3fb948e637817eb8351c68d177e', 0, '2017-03-26 17:16:38', NULL, NULL, NULL, NULL, NULL),
(73, 'sssingaaaaa', 'f7a9e24777ec23212c54d7a350bc5bea5477fdbb', 'aaaaaaaaa', 'aaaaaaaaaaa', '男', 'linsinginaag1234@gmail.com', 'aaa', 'cfa56666f8c9851dfb93915edfe964bc', 0, '2017-03-26 17:25:25', NULL, NULL, NULL, NULL, NULL),
(74, 'singinglincccc', 'a59e375e7e163c060ec5103e61f24bf008661a68', 'ccccc', 'cccc', '男', 'linsincccging1234@gmail.com', 'ccccccc', '203bd26194ebcd0f4184715a78c8279a', 0, '2017-03-26 17:26:32', NULL, NULL, NULL, NULL, NULL);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `account` (`account`),
  ADD UNIQUE KEY `nickname` (`nickname`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `members`
--
ALTER TABLE `members`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
