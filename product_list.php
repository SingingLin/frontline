<?php
require __DIR__ . '/__connect_db.php';
$pname = "product_list";

$per_page = 8;

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0;
$g_cate = isset($_GET['g_cate']) ? intval($_GET['g_cate']) : 0;
$search = isset($_GET['search']) ? $_GET['search'] : '';

//大分類與子分類1
$big_a_sql = "SELECT * FROM `categories` WHERE  `parent_sid`=0";
$big_a_rs = $mysqli->query($big_a_sql);

$sml_a_sql = "SELECT * FROM `categories_chid` WHERE  `prod_cate_sid`=0";
$sml_a_rs = $mysqli->query($sml_a_sql);

//大分類與子分類2
$big_b_sql = "SELECT * FROM `categories` WHERE  `parent_sid`=1";
$big_b_rs = $mysqli->query($big_b_sql);

$sml_b_sql = "SELECT * FROM `categories_chid` WHERE  `prod_cate_sid`=1";
$sml_b_rs = $mysqli->query($sml_b_sql);

//大分類與子分類3
$big_c_sql = "SELECT * FROM `categories` WHERE  `parent_sid`=2";
$big_c_rs = $mysqli->query($big_c_sql);

$sml_c_sql = "SELECT * FROM `categories_chid` WHERE  `prod_cate_sid`=2";
$sml_c_rs = $mysqli->query($sml_c_sql);

//大分類與子分類4
$big_d_sql = "SELECT * FROM `categories` WHERE  `parent_sid`=3";
$big_d_rs = $mysqli->query($big_d_sql);

$sml_d_sql = "SELECT * FROM `categories_chid` WHERE  `prod_cate_sid`=3";
$sml_d_rs = $mysqli->query($sml_d_sql);

//大分類與子分類5
$big_e_sql = "SELECT * FROM `categories` WHERE  `parent_sid`=4";
$big_e_rs = $mysqli->query($big_e_sql);

$sml_e_sql = "SELECT * FROM `categories_chid` WHERE  `prod_cate_sid`=4";
$sml_e_rs = $mysqli->query($sml_e_sql);

//大分類與子分類6
$big_f_sql = "SELECT * FROM `categories` WHERE  `parent_sid`=5";
$big_f_rs = $mysqli->query($big_f_sql);

$sml_f_sql = "SELECT * FROM `categories_chid` WHERE  `prod_cate_sid`=5";
$sml_f_rs = $mysqli->query($sml_f_sql);

$where = "WHERE 1";
$where_gun = " ";

if ($cate) {
    $where .= " AND `category_sid`=$cate ";
}
if ($g_cate) {
    $where_gun = " AND `gun_sid`=$g_cate ";
}
if ($search) {
    $search2 = $mysqli->escape_string("%{$search}%");
    $where .= sprintf(" AND `name` LIKE '%s' ", $search2);
}


$total_sql = "SELECT COUNT(1) FROM `products`$where $where_gun ";
$total_rs = $mysqli->query($total_sql);
$total_num = $total_rs->fetch_row()[0];
$total_page = ceil($total_num / $per_page);


$p_sql = sprintf("SELECT * FROM `products` %s $where_gun  LIMIT %s,%s", $where, ($page - 1) * $per_page, $per_page);
$p_rs = $mysqli->query($p_sql);


/*
$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0;
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$search = isset($_GET['search']) ? $_GET['search'] : '';

$c_sql = "SELECT * FROM `categories` WHERE `parent_sid`=0";
$c_rs = $mysqli->query($c_sql);

$where = "WHERE 1";

if ($cate) {
    $where .= " AND `category_sid`=$cate ";
}
if ($search) {
    $search2 = $mysqli->escape_string("%{$search}%");
    $where .= sprintf(" AND (`author` LIKE '%s' OR `bookname` LIKE '%s')",$search2,$search2);
}

$total_sql = "SELECT COUNT(1) FROM `products` $where ";
$total_rs = $mysqli->query($total_sql);
$total_num = $total_rs->fetch_row()[0];

$total_pages = ceil($total_num / $per_page);

$p_sql = sprintf("SELECT * FROM `products` %s LIMIT %s,%s",$where, ($page - 1) * $per_page, $per_page);

$p_rs = $mysqli->query($p_sql);
*/


?>

<?php include __DIR__ . '/__html_head.php' ?>
    <style>
        .font {
            font-size: 2em;
            color: red;
        }

        .link {
            color: yellow;
        }
    </style>
    <div class="container">

<?php include __DIR__ . '/__navbar.php' ?>

    <div class="row">
        <div class="col-md-3">
            <!--         分類1-->
            <form class="navbar-form navbar-left">
                <div class="form-group"><input class="form-control" name="search" placeholder="Search"
                    value="<?= empty($search) ? '': $search ?>"></div>
                <button type="submit" class="btn btn-default">搜尋</button>
            </form>
            <?php while ($row = $big_a_rs->fetch_assoc()): ?>
                <a class="col-md-12 font" href="?cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php while ($row = $sml_a_rs->fetch_assoc()): ?>
                    <a class="col-md-12" href="?g_cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php endwhile; ?>
            <?php endwhile; ?>

            <!--            分類2-->
            <?php while ($row = $big_b_rs->fetch_assoc()): ?>
                <a class="col-md-12 font" href="?cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php while ($row = $sml_b_rs->fetch_assoc()): ?>
                    <a class="col-md-12" href="?g_cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php endwhile; ?>
            <?php endwhile; ?>

            <!--            分類3-->
            <?php while ($row = $big_c_rs->fetch_assoc()): ?>
                <a class="col-md-12 font" href="?cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php while ($row = $sml_c_rs->fetch_assoc()): ?>
                    <a class="col-md-12" href="?g_cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php endwhile; ?>
            <?php endwhile; ?>

            <!--            分類4-->
            <?php while ($row = $big_d_rs->fetch_assoc()): ?>
                <a class="col-md-12 font" href="?cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php while ($row = $sml_d_rs->fetch_assoc()): ?>
                    <a class="col-md-12" href="?g_cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php endwhile; ?>
            <?php endwhile; ?>

            <!--            分類5-->
            <?php while ($row = $big_e_rs->fetch_assoc()): ?>
                <a class="col-md-12 font" href="?cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php while ($row = $sml_e_rs->fetch_assoc()): ?>
                    <a class="col-md-12" href="?g_cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php endwhile; ?>
            <?php endwhile; ?>

            <!--            分類6-->
            <?php while ($row = $big_f_rs->fetch_assoc()): ?>
                <a class="col-md-12 font" href="?cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php while ($row = $sml_f_rs->fetch_assoc()): ?>
                    <a class="col-md-12" href="?g_cate=<?= $row['sid'] ?>"><?= $row['name'] ?></a>
                <?php endwhile; ?>
            <?php endwhile; ?>

        </div>

        <div class="col-md-9">
            <div class="col-md-6">
                <nav>
                    <ul class="pagination">


                        <li><a href="" aria-label="Previous"><span
                                        aria-hidden="true">«</span></a>

                            <?php for ($i = 1;
                            $i <= $total_page;
                            $i++):
                            $ar = array('page' => $i);
                            if ($cate) {
                                $ar['cate'] = $cate;
                            }
                            if ($g_cate) {
                                $ar['g_cate'] = $g_cate;
                            }
                            ?>


                        <li class="link"><a href="?<?= http_build_query($ar) ?>" name="page" class="link"><span
                                        class="sr-only"></span><?= $i ?></a></li>
                        <?php endfor ?>

                        <li><a href="" aria-label="Next"><span
                                        aria-hidden="true">»</span></a></li>

                    </ul>
                </nav>
            </div>
            <div class="col-md-12"></div>
            <?php while ($row = $p_rs->fetch_assoc()): ?>
                <div class="col-md-3">
                    <div class="thumbnail" style="height:280px; margin:10px 0;">
                        <a class="single_product" href="">
                            <img src="imgs/small/<?= $row['head_image'] ?>.jpeg" style="width:100%;height: 100px;">
                        </a>
                        <div class="caption">
                            <h5><?= $row['name'] ?></h5>
                            <p>
                                <span class="font">$<?= $row['price'] ?></span><br>
                                <select name="qty" class="qty">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                </select>
                                <button class="btn btn-warning btn-lg buy_btn pull-right" data-sid="<?= $row['sid'] ?>">買</button>
                                <button class="glyphicon glyphicon-heart btn-lg pull-right" data-sid=""></button>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>


        </div>

    </div>
    <script>
        $(function () {
            $('.buy_btn').click(function () {
                var sid = $(this).attr('data-sid');
                var qty = $(this).closest('.caption').find('.qty').val();

                $.get('add_to_cart.php', {sid:sid,qty:qty},function (data) {
                    calc_items(data);
                },'json');
            });

        });



    </script>
<?php include __DIR__ . '/__html_foot.php' ?>