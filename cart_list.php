<?php
require __DIR__ . '/__connect_db.php';
$pname = 'cart_list';

if (!empty($_SESSION['cart'])) {

    $sids = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $sids));

    $rs = $mysqli->query($sql);

    $cart_data = array();

    while ($row = $rs->fetch_assoc()) {
        $row['qty'] = $_SESSION['cart'][$row['sid']];
        $cart_data[$row['sid']] = $row;
    }
}

5456456456
/*
if (!empty($_SESSION['cart'])) {

    $sids = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $sids));

    $rs = $mysqli->query($sql);

    $cart_data = array();

    while ($row = $rs->fetch_assoc()) {
        $row['qty'] = $_SESSION['cart'][$row['sid']];
        $cart_data[$row['sid']] = $row;

    }
}
*/
?>

    <div class="container">
        <?php include __DIR__ . '/__html_head.php' ?>
        <style>
            .remove_item {
                font-size: x-large;
                cursor: pointer;
            }


        </style>


        <?php include __DIR__ . '/__navbar.php' ?>


        <div class="col-md-12">
            <?php if (isset($_SESSION['cart'])): ?>
                <table class="table table-bordered">
                    <thead>

                    <tr>
                        <th>商品</th>
                        <th>商品名稱</th>
                        <th>規格</th>
                        <th>數量</th>
                        <th>價格</th>
                        <th>總計</th>
                        <th>變更</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total = 0;
                    foreach ($_SESSION['cart'] as $sid => $qty):
                        $row = $cart_data[$sid];
                        $total += $row['price']*$row['qty'];
                        ?>
                    <tr data-sid="<?= $row['sid'] ?>">
                        <td><img style="width: 170px;height: 100px;" src="imgs/small/<?= $row['head_image'] ?>.jpeg" alt=""></td>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['description_1'] ?><br>
                            <?= $row['description_2'] ?><br>
                            <?= $row['description_3'] ?><br>
                            <?= $row['description_4'] ?><br>
                            <?= $row['description_5'] ?><br>
                        </td>
                        <td>
                            <select name="qty" class="qty" data-qty="<?= $row['qty'] ?>">
                                <?php for ($i=1;$i<10;$i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                                <?php endfor; ?>
                            </select>

                        </td>
                        <td class="price"><?= $row['price'] ?></td>
                        <td class="subtotal"><?= $row['price'] * $row['qty'] ?></td>
                        <td class="change">
                            <span class="glyphicon glyphicon-remove btn remove_item" aria-hidden="true"></span>
                            <span class="glyphicon glyphicon-heart btn like_item" aria-hidden="true"></span>

                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="col-md-12">
                    <div class="pull-right">總計:
                        <strong class="total_price "><?= $total ?></strong></div>
                </div>
                <div class="col-md-12">
                    <a href="checkout.php" class="btn btn-primary pull-right">結帳</a>
                    <a href="product_list.php" class="btn btn-primary pull-right">繼續購物</a>
                </div>
            <?php else: ?>


            <?php endif; ?>


        </div>
    </div>

    <script>
        $('.remove_item').click(function () {
            var tr = $(this).closest('tr');
            var sid = tr.attr("data-sid");

            $.get('add_to_cart.php',{sid:sid},function (data) {
                tr.remove();
                calc_items(data);
                calc_total_price()
            },'json');
        });

        var qty_sels = $('select.qty');

        qty_sels.each(function () {
            var qty = $(this).attr('data-qty');
            $(this).val(qty);
        });

        qty_sels.change(function () {
            var $tr = $(this).closest('tr');
            var sid = $tr.attr("data-sid");
            var qty = $(this).val();
            var price = $tr.find('.price').text();
            var $subtotal = $tr.find('.subtotal');

            $.get('add_to_cart.php',{sid:sid,qty:qty},function (data) {
                $subtotal.text(qty*price);
                calc_items(data);
                calc_total_price()
            },'json')
        })

        function calc_total_price() {
            var t = 0
            $('td.subtotal').each(function () {
                t += parseInt($(this).text());
            });
            $('.total_price').text(t)
        }


    </script>

<?php include __DIR__ . '/__html_foot.php' ?>