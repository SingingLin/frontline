-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2017-03-28 10:57:12
-- 伺服器版本: 10.1.21-MariaDB
-- PHP 版本： 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `frontline`
--

-- --------------------------------------------------------

--
-- 資料表結構 `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `intorduction` varchar(255) NOT NULL,
  `description_1` varchar(255) NOT NULL,
  `description_2` varchar(255) NOT NULL,
  `description_3` varchar(255) NOT NULL,
  `description_4` varchar(255) NOT NULL,
  `description_5` varchar(255) NOT NULL,
  `head_image` varchar(255) NOT NULL,
  `image_1` varchar(255) NOT NULL,
  `image_2` varchar(255) NOT NULL,
  `image_3` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `host` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `ground` enum('1','2','3','4') NOT NULL,
  `mode` enum('1','2','3','4') NOT NULL,
  `equip_1` enum('1','2','3','4') NOT NULL,
  `equip_2` enum('1','2','3','4') NOT NULL,
  `equip_3` enum('1','2','3','4') NOT NULL,
  `equip_4` enum('1','2','3','4') NOT NULL,
  `total_people` int(11) NOT NULL,
  `active_note` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `products`
--

INSERT INTO `products` (`id`, `type`, `name`, `price`, `intorduction`, `description_1`, `description_2`, `description_3`, `description_4`, `description_5`, `head_image`, `image_1`, `image_2`, `image_3`, `category_id`, `host`, `date`, `ground`, `mode`, `equip_1`, `equip_2`, `equip_3`, `equip_4`, `total_people`, `active_note`) VALUES
(1, 1, '一芝軒 ICS AARF 沙色 輕量折托版 次世代折疊托電動槍', 6320, '雙邊射擊選擇鈕以及雙邊槍機拉柄，讓左右撇子都能運用自如\r\n\r\n防滑護木設計加上防滑人體工學握把，大大減輕持槍時的疲憊感\r\n\r\n通用M4系列彈匣，除了減輕負擔，更是提高通用率!\r\n\r\n ', '全長：746mm-716mm', '折疊後長度：516mm', '內管長度：263mm', '初速：120m/s', '彈匣容量：300發', 'image_1', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(2, 1, 'MILSIG M17 CQC 國際版 單連發 戰術漆彈鎮暴槍(17MM)，HEATCORE系統', 11800, '保留2013年 M系列旗艦優點，2014年槍款全面搭載更新的 HEATCORE系統，\r\n\r\n槍身無論複合材料槍款 或 全金屬槍款均增加故障排除拉柄。\r\n\r\n(槍機拉柄外型，直接拉拉柄強制推彈機構復歸，可於故障時執行快速故障排除)\r\n\r\n\r\n另搭載2014年 新款機械式 單連發M4真槍採寸握把，\r\n\r\n只需撥動射擊選擇鈕即可完成保險、單發、全自動射擊模式。\r\n\r\n\r\n2015年樣式 M17 CQC 全新升級採用真品級複合材料槍身，\r\n\r\n槍身內部包覆鋼骨架構，槍身耐壓、耐衝擊程度與全金屬版本相當。 ', 'M17系列 CQC 複合材料槍身本體', '塑料折疊瞄具', '18發圓球尾翼彈兩用彈匣', '彈匣外型防塵蓋(假彈匣)', 'CQC尼龍護木', 'image_2', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(3, 1, 'M120 狙擊版~HFC VSR11 手拉空氣長槍', 5980, 'M120 狙擊版~HFC VSR11 手拉空氣長槍\r\n初速 !!  120-125 m/s\r\n\r\n附 3-9倍 狙擊鏡，M3伸縮腳架\r\n\r\n超長金屬內外槍管，可調式HOP-UP系統 \r\n上推式保險卡損，讓您上膛後不會誤擊，安全度100分\r\n強化橡膠製槍托底板\r\n\r\n全長107.5公分！', '填彈器', '說明書', '狙擊鏡', '腳架', '測試用 BB彈500發', 'image_3', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(4, 0, 'APS BOAR Defense Ambi EBB 電動步槍(3Gun 授權刻印)', 8300, 'CNC 全金屬槍身，\"3 Gun Nation\"授權刻印\r\n415mm KeyMod 金屬魚骨護木\r\n黃金防火帽\r\n黃金槍機飾片(可動)\r\n黃金槍機釋放鈕(網球拍)\r\nRAF黃金7字直型扳機\r\n黃金射擊選擇紐\r\n黃金雙邊彈匣釋放鈕\r\n45度光纖戰術摺疊準星照門', '重量：3200 g', '長度：830 / 895 mm', 'Hop-up：可調式', '射擊選擇模式：保險/單發/連發', '彈匣容量：300 Rounds', 'image_4', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(5, 1, 'G&G 怪怪 GC16 9吋 電子控制開關 三發點放 全金屬電動槍，電槍', 8000, '在重覆一次動作即轉換回連發。\r\n段式伸縮槍托(GOS-V3)：獨特卡榫設計，將槍托調整你所需要的位置，\r\n釋放桿鎖定，槍托即不會移動，穩定性大幅提升。\r\n雙邊彈匣釋放鈕\r\n8mm 培林 BOX\r\n25000rpm 高扭力長軸\r\n可拆式前準星、可拆式後罩門\r\n金屬軌道系統組、強化玻璃纖維槍身\r\n300發高容量彈匣(GMAG-V1)', '材質：鋁合金/ 不鏽鋼 / 尼龍加纖 / 鋼鐵 / 鋅合金 / 塑膠', '總長：808 公厘', '重量：2667 克 ', '馬達：25000rpm 橘強磁長軸', '內管長度：260 公厘', 'image_5', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(6, 1, 'HFC 全金屬盒子砲瓦斯槍，BB槍(復古軍用毛瑟槍)', 12000, '超優質感，使用6mm BB彈，屬於半自動滑套固定式，只需扣板機，即可發射!!\r\n不論是手感，外型，擊發時的快感，您絕不可錯過。\r\n喜好收藏的朋友，這支豈能錯過!!\r\n全金屬材質\r\n半自動，滑套固定式\r\n可調式表尺，擊槌可固定在後，隨時可擊發', '尺寸：28 cm x 15 cm', '初速: 134 m/s(0.2g)', '重量： 1.2 kg', '裝彈數：26 發', '盒子砲瓦斯槍 x 1', 'image_6', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(7, 1, 'M120 狙擊版~HFC VSR11 手拉空氣長槍', 5980, 'M120 狙擊版~HFC VSR11 手拉空氣長槍\r\n初速 !!  120-125 m/s\r\n\r\n附 3-9倍 狙擊鏡，M3伸縮腳架\r\n\r\n超長金屬內外槍管，可調式HOP-UP系統 \r\n上推式保險卡損，讓您上膛後不會誤擊，安全度100分\r\n強化橡膠製槍托底板\r\n\r\n全長107.5公分！', '填彈器', '說明書', '狙擊鏡', '腳架', '測試用 BB彈500發', 'image_7', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(8, 0, 'APS BOAR Defense Ambi EBB 電動步槍(3Gun 授權刻印)', 8300, 'CNC 全金屬槍身，\"3 Gun Nation\"授權刻印\r\n415mm KeyMod 金屬魚骨護木\r\n黃金防火帽\r\n黃金槍機飾片(可動)\r\n黃金槍機釋放鈕(網球拍)\r\nRAF黃金7字直型扳機\r\n黃金射擊選擇紐\r\n黃金雙邊彈匣釋放鈕\r\n45度光纖戰術摺疊準星照門', '重量：3200 g', '長度：830 / 895 mm', 'Hop-up：可調式', '射擊選擇模式：保險/單發/連發', '彈匣容量：300 Rounds', 'image_8', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(9, 1, 'G&G 怪怪 GC16 9吋 電子控制開關 三發點放 全金屬電動槍，電槍', 8000, '在重覆一次動作即轉換回連發。\r\n段式伸縮槍托(GOS-V3)：獨特卡榫設計，將槍托調整你所需要的位置，\r\n釋放桿鎖定，槍托即不會移動，穩定性大幅提升。\r\n雙邊彈匣釋放鈕\r\n8mm 培林 BOX\r\n25000rpm 高扭力長軸\r\n可拆式前準星、可拆式後罩門\r\n金屬軌道系統組、強化玻璃纖維槍身\r\n300發高容量彈匣(GMAG-V1)', '材質：鋁合金/ 不鏽鋼 / 尼龍加纖 / 鋼鐵 / 鋅合金 / 塑膠', '總長：808 公厘', '重量：2667 克 ', '馬達：25000rpm 橘強磁長軸', '內管長度：260 公厘', 'image_9', '', '', '', 0, '', '0000-00-00', '', '', '', '', '', '', 0, ''),
(10, 1, 'HFC 全金屬盒子砲瓦斯槍，BB槍(復古軍用毛瑟槍)', 12000, '超優質感，使用6mm BB彈，屬於半自動滑套固定式，只需扣板機，即可發射!!\r\n不論是手感，外型，擊發時的快感，您絕不可錯過。\r\n喜好收藏的朋友，這支豈能錯過!!\r\n全金屬材質\r\n半自動，滑套固定式\r\n可調式表尺，擊槌可固定在後，隨時可擊發', '尺寸：28 cm x 15 cm', '初速: 134 m/s(0.2g)', '重量： 1.2 kg', '裝彈數：26 發', '盒子砲瓦斯槍 x 1', 'image_10', '', '', '', 1, '', '0000-00-00', '', '', '', '', '', '', 0, '');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
