-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2017-03-28 16:22:32
-- 伺服器版本: 10.1.21-MariaDB
-- PHP 版本： 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `frontline`
--

-- --------------------------------------------------------

--
-- 資料表結構 `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_sid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_sid`) VALUES
(1, '武器系統\r\n', 0),
(2, '長槍\r\n', 0),
(3, '機槍\r\n', 0),
(4, '短槍\r\n', 0),
(5, '武器系統配件\r\n', 1),
(6, '槍管護木\r\n', 1),
(7, '握把\r\n', 1),
(8, '瞄準鏡\r\n', 1),
(9, '槍燈/彈匣\r\n', 1),
(10, '槍托/槍背帶\r\n', 1),
(11, '電動槍電池\r\n', 1),
(12, '瓦斯槍OC2鋼瓶\r\n', 1),
(13, '武器系統零件\r\n', 1),
(14, '齒輪\r\n', 1),
(15, '槍內管\r\n', 1),
(16, '槍機\r\n', 1),
(17, '配件\r\n', 2),
(18, '頭部配件\r\n', 2),
(19, '手套\r\n', 2),
(20, '背包\r\n', 2),
(21, '臂章\r\n', 2),
(22, '槍袋\r\n', 2),
(23, '彈藥庫\r\n', 2),
(24, '男性\r\n', 3),
(25, '上衣\r\n', 3),
(26, '戰鬥背心\r\n', 3),
(27, '長褲\r\n', 3),
(28, '特殊服裝 \r\n', 3),
(29, '鞋/靴類\r\n', 3),
(30, '女性', 4),
(31, '上衣\r\n', 4),
(32, '戰鬥背心\r\n', 4),
(33, '長褲\r\n', 4),
(34, '特殊服裝 \r\n', 4),
(35, '鞋/靴類\r\n', 4);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
