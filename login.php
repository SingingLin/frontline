<?php
require __DIR__ . '/__connect_db.php';
$pname = 'login';
$title = '會員登入';
$success = false;

//if(isset($_SESSION['user'])){
//    header('Location:./');
//    exit();
//}

if( isset( $_POST['email'] ) ){
    $sql = sprintf("SELECT * FROM `members` WHERE `email`='%s' AND `password`='%s'",
        $mysqli->escape_string($_POST['email']),
        sha1($_POST['password'])
    );

    $result = $mysqli->query($sql);
    if($result->num_rows){
      $row = $result->fetch_assoc();

      $_SESSION['user'] = $row;

    };

};




?>

<?php include  __DIR__. '/__html_head.php'; ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

<?php if(isset($result)): ?>
    <?php if($result->num_rows): ?>
        <?php header('Location:./'); ?>
    <?php else: ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                帳號或密碼錯誤
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>



<?php if(!(isset($result) AND $result->num_rows)): ?>
<div class="" style="">
    <div class="panel-heading"><h3 class="panel-title">登入</h3></div>
    <div class="panel-body">

        <form name="form1" method="post" onsubmit="return checkForm();">

            <div class="form-group">
                <label for="email">E-mail<span class="info"></span></label>
                <input type="text" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="password">密碼<span class="info"></span></label>
                <input type="password" class="form-control" id="password" name="password">
            </div>

            <a href="register.php"><div type="submit" class="btn btn-default">加入會員</div></a>
            <button type="submit" class="btn btn-default">登入</button>
            <br/>
            <a href="forgot_password.php"> 忘記密碼 </a>

        </form>


    </div>
</div>
    <?php endif; ?>

</div>
<?php include  __DIR__. '/__html_foot.php'; ?>