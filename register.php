<?php
require __DIR__ . '/__connect_db.php';
$pname = 'register';
$title = '會員註冊';
$success = false;



if(isset($_POST['email'])) {
//    echo '<pre>';
//    print_r($_POST);
//    echo '</pre>';

    $hash = md5( $_POST['email']. uniqid() );


    $sql = "INSERT INTO `members`(`sid`,
    `email`, `password`, `name`, 
    `gender`, `phone`, `address`, 
    `hash`, `created_at`) VALUES 
    ( NULL,
    ?, ?, ?, 
    ?, ?, ?, 
    ?, NOW() )" ;
//  echo $sql;
//  exit;

    $stmt = $mysqli->prepare($sql);
    $stmt -> bind_param('sssssss',
        $_POST['email'], sha1($_POST['password']), $_POST['name'],
        $_POST['gender'], $_POST['phone'], $_POST['address'],
        $hash
    );

    $success = $stmt->execute();

    //$mysqli->errno
    //$mysqli->error
    //exit; //die();
}


if($success) {
    header("Location: login.php");
};

//if(isset($_SESSION['user'])){
//    header('Location: ./');
//    exit;
//}
//
//
//if(isset($_POST['email'])) {
//    echo '<!-- <pre>';
//    print_r($_POST);
//    echo '</pre> -->';
//
//    $sql = sprintf("SELECT * FROM `members` WHERE `email`='%s'",
//        $_POST['email'] );
////    echo $sql;
////    exit;
//
//    $rs = $mysqli->query($sql);
//    if($rs->num_rows) {
//        $row = $rs->fetch_assoc();
//
//        $_SESSION['user'] = $row;
//    }
//}


?>


<?php include  __DIR__. '/__html_head.php'; ?>
    <style>
        .info {
            color: red;
            font-weight: bold;
        }
        ::-webkit-input-placeholder {
            color: red;
        }
    </style>
<div class="container">
    <?php include __DIR__ . '/__navbar.php'; ?>

<!--    --><?php //if($success): ?>
<!--    <div class="col-md-12">-->
<!--        <div class="alert alert-success" role="alert">-->
<!--            註冊完成-->
<!--        </div>-->
<!--    </div>-->
<!--    --><?php //endif; ?>
    <div class="col-md-6">

        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">會員註冊</h3></div>
                <div class="panel-body">

                    <form name="form1" method="post" onsubmit="return checkForm();">

                        <div class="form-group">
                            <label for="email">E-mail<span class="info"></span></label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>

                        <div class="form-group">
                            <label for="password">密碼<span class="info"></span></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>

                        <div class="form-group">
                            <label for="password_confirm">密碼確認<span class="info"></span></label>
                            <input type="password" class="form-control" id="password_confirm" name="password_confirm">
                        </div>

                        <div class="form-group">
                            <label for="name">姓名<span class="info"></span></label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="請填入真實姓名">
                        </div>

                        <div class="form-group">
                            <label for="gender">性別<span class="info"></span></label>
                            <select name="gender" class="gender" id="gender">
                                <option value="1">男</option>
                                <option value="2">女</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="phone">手機<span class="info"></span></label>
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>

                        <div class="form-group">
                            <label for="address">地址<span class="info"></span></label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>

                        <button type="submit" class="btn btn-default">註冊</button>
                    </form>


                </div>
            </div>

        </div>


    </div>


</div>
    <script>


        function checkForm(){
            var $email = $('#email');
            var $password = $('#password');
            var $password_confirm = $('#password_confirm');
            var $name = $('#name');
            var $phone = $('#phone');
            var $address = $('#address');

            var items = [$email, $password, $password_confirm, $name, $phone, $address];

            var email = $email.val();
            var password = $password.val();
            var password_confirm = $password_confirm.val();
            var name = $name.val();
            var phone = $phone.val();
            var address = $address.val();

            var i;
            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var isPass = true;



            for(i=0; i<items.length; i++){
                items[i].closest('.form-group').find('.info').text('');
                items[i].css('border-color', '#ccc');
            }

            if(! pattern.test(email)){
                $email.closest('.form-group').find('.info').text(' E-mail格式不正確 !');
                $email.css('border-color', 'red');
                isPass = false;
            }

            if(! email){
                $email.closest('.form-group').find('.info').text(' 請填寫E-mail !');
                $email.css('border-color', 'red');
                isPass = false;
            }

            if(password.length < 6){
                $password.closest('.form-group').find('.info').text(' 密碼至少 6 個字元 !');
                $password.css('border-color', 'red');
                isPass = false;
            }

            if(! (password_confirm == password) ){
                $password_confirm.closest('.form-group').find('.info').text(' 密碼不相符 !');
                $password_confirm.css('border-color', 'red');
                isPass = false;
            }

            if(! password_confirm){
                $password_confirm.closest('.form-group').find('.info').text(' 請再輸入一次密碼 !');
                $password_confirm.css('border-color', 'red');
                isPass = false;
            }

            if(name.length < 2){
                $name.closest('.form-group').find('.info').text(' 請填寫真實姓名!');
                $name.css('border-color', 'red');
                isPass = false;
            }


            if(! phone){
                $phone.closest('.form-group').find('.info').text(' 請填寫電話 !');
                $phone.css('border-color', 'red');
                isPass = false;
            }

            if(! address){
                $address.closest('.form-group').find('.info').text(' 請填寫地址 !');
                $address.css('border-color', 'red');
                isPass = false;
            }


            $.get('aj_check_email.php', {email: email}, function(data){
                if(data >= '1'){
                    $email.closest('.form-group').find('.info').text('此 email 已經使用過了!');
                    $email.css('border-color', 'red');
                    isPass = false;
                }

                if(isPass){
                    document.form1.submit();
                }

            });


            return false;



        }



    </script>
<?php include  __DIR__. '/__html_foot.php'; ?>